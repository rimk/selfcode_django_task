# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A/B split test significance calculator
* Django 2.0
* [Heroku Link](https://cryptic-meadow-51053.herokuapp.com/)

### SETUP ###

* To install the project, install pip and run the following command:
`pip install -r requirements.txt`
* To start the project run
`python manage.py runserver`
* Dependencies are listed in requirements.txt file

### Contact ###

* Abdou Karim KANDJI (karim_155@hotmail.com)