# coding=utf-8
import logging
from urllib.error import URLError

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from math import sqrt

from scipy.stats import norm

log = logging.getLogger(__name__)


def index(request):
    return render(request, 'index.html', )


def significance_calculator(request):
    """
    Statistic significance calculator
    :param request:
    :return: p_value, significance
    """
    if request.is_ajax():
        # Get params sent with AJAX
        control_conversions = int(request.GET.get('id_control_conversions'))
        control_visitors = int(request.GET.get('id_control_visitors'))
        variation_conversions = int(request.GET.get('id_variation_conversions'))
        variation_visitors = int(request.GET.get('id_variation_visitors'))

        # compute control & variation rates
        control_rate = (control_conversions/control_visitors)
        variation_rate = (variation_conversions/variation_visitors)
        try:
            control_se = sqrt((control_rate*(1-control_rate)/control_visitors))
        except ValueError:
            control_se = None
        try:
            variation_se = sqrt((variation_rate*(1-variation_rate)/variation_visitors))
        except ValueError:
            variation_se = None

        try:
            z_score = (control_rate-variation_rate)/sqrt(control_se**2+variation_se**2)
            if z_score < 0:
                z_score *= -1
        except ZeroDivisionError:
            z_score = None
        except TypeError:
            z_score = None

        try:
            # calculate p_value based on z_score and round it with 3 decimal digits
            p_value = round(float(1 - norm.cdf(z_score)), 3)
        except TypeError:
            p_value = None

        try:
            # determine significance with p_value
            significance = "Yes!" if (p_value < 0.1 or p_value > 0.9) else "No"
        except TypeError:
            significance = "No"

        return HttpResponse(JsonResponse(data={'p_value': p_value, 'significance': significance}))
    else:
        raise URLError('HttpResponseError')

